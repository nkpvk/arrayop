from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray

def compare_1d_arrays(first, second):
    for i in range(1, first.shape[0]+1):
        if first[i] != second[i]:
            return False
    return True

def compare_2d_arrays(first, second):
    for row in range(1, first.shape[0]+1):
        for col in range(1, first.shape[1]+1):
            if first[row, col] != second[row, col]:
                return False
    return True

class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    def test_print(self):
        a = BaseArray((3,))
        a[1] = 1
        a[2] = 1000
        a[3] = -5

        self.assertEqual(a.to_string(), "    1.0 1000.0   -5.0\n")

        a = BaseArray((2, 3))
        a[1, 1] = -3
        a[1, 2] = -2
        a[1, 3] = -1
        a[2, 1] = 0
        a[2, 2] = 1
        a[2, 3] = 2

        self.assertEqual(a.to_string(), "   -3.0   -2.0   -1.0\n    0.0    1.0    2.0\n")

        a = BaseArray((2, 3), dtype=float)
        a[1, 1] = 3.14
        a[1, 2] = -2.7
        a[1, 3] = 1.33
        a[2, 1] = 0.2
        a[2, 2] = -1.02
        a[2, 3] = 2.35

        self.assertEqual(a.to_string(), "   3.14   -2.7   1.33\n    0.2  -1.02   2.35\n")

    def test_sort(self):
        a = BaseArray((5,))
        a[1] = 10
        a[2] = 21
        a[3] = -123
        a[4] = 65
        a[5] = 2

        expected_result = BaseArray((5,))
        expected_result[1] = -123
        expected_result[2] = 2
        expected_result[3] = 10
        expected_result[4] = 21
        expected_result[5] = 65

        self.assertTrue(compare_1d_arrays(a.sort(), expected_result))
        self.assertTrue(compare_1d_arrays(a.sort(False), expected_result))

        self.assertFalse(compare_1d_arrays(a.sort(), a))
        self.assertFalse(compare_1d_arrays(a.sort(False), a))

        # =====================================================================

        a = BaseArray((3,))
        a[1] = 3.14
        a[2] = 2.7
        a[3] = -0.64

        expected_result = BaseArray((3,))
        expected_result[1] = -0.64
        expected_result[2] = 2.7
        expected_result[3] = 3.14

        self.assertTrue(compare_1d_arrays(a.sort(), expected_result))
        self.assertTrue(compare_1d_arrays(a.sort(False), expected_result))

        self.assertFalse(compare_1d_arrays(a.sort(), a))
        self.assertFalse(compare_1d_arrays(a.sort(False), a))

        # =====================================================================

        a = BaseArray((3, 3))
        a[1, 1] = 0
        a[1, 2] = 1
        a[1, 3] = 1
        a[2, 1] = 4
        a[2, 2] = 0
        a[2, 3] = -2
        a[3, 1] = 1
        a[3, 2] = 3
        a[3, 3] = 0

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 0
        expected_result[1, 2] = 1
        expected_result[1, 3] = 1
        expected_result[2, 1] = -2
        expected_result[2, 2] = 0
        expected_result[2, 3] = 4
        expected_result[3, 1] = 0
        expected_result[3, 2] = 1
        expected_result[3, 3] = 3

        self.assertTrue(compare_2d_arrays(a.sort(), expected_result))
        self.assertFalse(compare_2d_arrays(a.sort(), a))

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 0
        expected_result[1, 2] = 0
        expected_result[1, 3] = -2
        expected_result[2, 1] = 1
        expected_result[2, 2] = 1
        expected_result[2, 3] = 0
        expected_result[3, 1] = 4
        expected_result[3, 2] = 3
        expected_result[3, 3] = 1

        self.assertTrue(compare_2d_arrays(a.sort(False), expected_result))
        self.assertFalse(compare_2d_arrays(a.sort(False), a))

    def test_find(self):
        a = BaseArray((3,))
        a[1] = 0
        a[2] = 2
        a[3] = 0

        self.assertEqual(a.find(0), [(1,), (3,)])
        self.assertEqual(a.find(5), [])

        a = BaseArray((3, 3))
        a[1, 1] = 0
        a[1, 2] = 0
        a[1, 3] = 0
        a[2, 1] = 2
        a[2, 2] = 0
        a[2, 3] = 0
        a[3, 1] = 0
        a[3, 2] = 2
        a[3, 3] = 2

        self.assertEqual(a.find(2), [(2, 1), (3, 2), (3, 3)])
        self.assertEqual(a.find(-1), [])

        a = BaseArray((2, 3))
        a[1, 1] = -3
        a[1, 2] = -2
        a[1, 3] = -1
        a[2, 1] = -2
        a[2, 2] = 1
        a[2, 3] = -2

        self.assertEqual(a.find(-2), [(1, 2), (2, 1), (2, 3)])
        self.assertEqual(a.find(3), [])

        a = BaseArray((2, 2, 2))
        a[1, 1, 1] = 0
        a[1, 1, 2] = -22
        a[1, 2, 1] = 10
        a[1, 2, 2] = 2
        a[2, 1, 1] = 0
        a[2, 1, 2] = 10
        a[2, 2, 1] = 10
        a[2, 2, 2] = 5

        self.assertEqual(a.find(10), [(1, 2, 1), (2, 1, 2), (2, 2, 1)])
        self.assertEqual(a.find(-22), [(1, 1, 2)])
        self.assertEqual(a.find(1), [])

    def test_add(self):
        a = BaseArray((2, 3))
        a[1, 1] = -3
        a[1, 2] = -2
        a[1, 3] = -1
        a[2, 1] = -2
        a[2, 2] = 1
        a[2, 3] = -2

        expected_result = BaseArray((2, 3))
        expected_result[1, 1] = 2
        expected_result[1, 2] = 3
        expected_result[1, 3] = 4
        expected_result[2, 1] = 3
        expected_result[2, 2] = 6
        expected_result[2, 3] = 3

        self.assertTrue(compare_2d_arrays(a.add(5), expected_result))
        self.assertFalse(compare_2d_arrays(a.add(3), expected_result))

        # =====================================================================

        b = BaseArray((2, 3))
        b[1, 1] = 3
        b[1, 2] = 6
        b[1, 3] = -1
        b[2, 1] = 20
        b[2, 2] = -10
        b[2, 3] = -2

        expected_result = BaseArray((2, 3))
        expected_result[1, 1] = 0
        expected_result[1, 2] = 4
        expected_result[1, 3] = -2
        expected_result[2, 1] = 18
        expected_result[2, 2] = -9
        expected_result[2, 3] = -4

        self.assertTrue(compare_2d_arrays(a.add(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.add(2), expected_result))

    def test_sub(self):
        a = BaseArray((2, 3))
        a[1, 1] = -3
        a[1, 2] = -2
        a[1, 3] = -1
        a[2, 1] = -2
        a[2, 2] = 1
        a[2, 3] = -2

        expected_result = BaseArray((2, 3))
        expected_result[1, 1] = -8
        expected_result[1, 2] = -7
        expected_result[1, 3] = -6
        expected_result[2, 1] = -7
        expected_result[2, 2] = -4
        expected_result[2, 3] = -7

        self.assertTrue(compare_2d_arrays(a.sub(5), expected_result))
        self.assertFalse(compare_2d_arrays(a.sub(2), expected_result))

        # =====================================================================

        b = BaseArray((2, 3))
        b[1, 1] = 3
        b[1, 2] = 6
        b[1, 3] = -1
        b[2, 1] = 20
        b[2, 2] = -10
        b[2, 3] = -2

        expected_result = BaseArray((2, 3))
        expected_result[1, 1] = -6
        expected_result[1, 2] = -8
        expected_result[1, 3] = 0
        expected_result[2, 1] = -22
        expected_result[2, 2] = 11
        expected_result[2, 3] = 0

        self.assertTrue(compare_2d_arrays(a.sub(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.sub(2), expected_result))

    def test_mult_scalar(self):
        a = BaseArray((3, 2))
        a[1, 1] = 1
        a[1, 2] = 2
        a[2, 1] = 3
        a[2, 2] = 4
        a[3, 1] = 5
        a[3, 2] = 6

        b = BaseArray((3, 2))
        b[1, 1] = 2
        b[1, 2] = 10
        b[2, 1] = 5
        b[2, 2] = 3
        b[3, 1] = -15
        b[3, 2] = 1

        expected_result = BaseArray((3, 2))
        expected_result[1, 1] = 2
        expected_result[1, 2] = 20
        expected_result[2, 1] = 15
        expected_result[2, 2] = 12
        expected_result[3, 1] = -75
        expected_result[3, 2] = 6

        self.assertTrue(compare_2d_arrays(a.mult_scalar(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.mult_scalar(2), expected_result))

    def test_mult(self):
        a = BaseArray((3, 2))
        a[1, 1] = 1
        a[1, 2] = 2
        a[2, 1] = 3
        a[2, 2] = 4
        a[3, 1] = 5
        a[3, 2] = 6

        expected_result = BaseArray((3, 2))
        expected_result[1, 1] = 2
        expected_result[1, 2] = 4
        expected_result[2, 1] = 6
        expected_result[2, 2] = 8
        expected_result[3, 1] = 10
        expected_result[3, 2] = 12

        self.assertTrue(compare_2d_arrays(a.mult(2), expected_result))
        self.assertFalse(compare_2d_arrays(a.mult(5), expected_result))

        # =====================================================================

        b = BaseArray((2, 3))
        b[1, 1] = 2
        b[1, 2] = 10
        b[1, 3] = 5
        b[2, 1] = 3
        b[2, 2] = -15
        b[2, 3] = 1

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 8.0
        expected_result[1, 2] = -20.0
        expected_result[1, 3] = 7.0
        expected_result[2, 1] = 18.0
        expected_result[2, 2] = -30.0
        expected_result[2, 3] = 19.0
        expected_result[3, 1] = 28.0
        expected_result[3, 2] = -40.0
        expected_result[3, 3] = 31.0

        self.assertTrue(compare_2d_arrays(a.mult(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.mult(2), expected_result))

        # =====================================================================

        c = BaseArray((2, 3))

        with self.assertRaises(Exception):
            c.mult(b)

    def test_div(self):
        a = BaseArray((3, 2))
        a[1, 1] = 1
        a[1, 2] = 2
        a[2, 1] = 3
        a[2, 2] = 4
        a[3, 1] = 5
        a[3, 2] = 6

        expected_result = BaseArray((3, 2))
        expected_result[1, 1] = 1/2
        expected_result[1, 2] = 2/2
        expected_result[2, 1] = 3/2
        expected_result[2, 2] = 4/2
        expected_result[3, 1] = 5/2
        expected_result[3, 2] = 6/2

        self.assertTrue(compare_2d_arrays(a.div(2), expected_result))
        self.assertFalse(compare_2d_arrays(a.div(5), expected_result))

        # =====================================================================

        b = BaseArray((3, 2))
        b[1, 1] = 2
        b[1, 2] = 10
        b[2, 1] = 5
        b[2, 2] = 2
        b[3, 1] = -2
        b[3, 2] = 1

        expected_result = BaseArray((3, 2))
        expected_result[1, 1] = 0.5
        expected_result[1, 2] = 0.2
        expected_result[2, 1] = 0.6
        expected_result[2, 2] = 2.0
        expected_result[3, 1] = -2.5
        expected_result[3, 2] = 6.0

        self.assertTrue(compare_2d_arrays(a.div(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.div(2), expected_result))

    def test_log(self):
        a = BaseArray((3, 3))
        a[1, 1] = 128
        a[1, 2] = 2048
        a[1, 3] = 4096
        a[2, 1] = 8
        a[2, 2] = 2
        a[2, 3] = 16
        a[3, 1] = 1024
        a[3, 2] = 512
        a[3, 3] = 4

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 7
        expected_result[1, 2] = 11
        expected_result[1, 3] = 12
        expected_result[2, 1] = 3
        expected_result[2, 2] = 1
        expected_result[2, 3] = 4
        expected_result[3, 1] = 10
        expected_result[3, 2] = 9
        expected_result[3, 3] = 2

        self.assertTrue(compare_2d_arrays(a.log(2), expected_result))
        self.assertFalse(compare_2d_arrays(a.log(5), expected_result))

        # =====================================================================

        b = BaseArray((3, 3))
        b[1, 1] = 2
        b[1, 2] = 4
        b[1, 3] = 2
        b[2, 1] = 4
        b[2, 2] = 2
        b[2, 3] = 4
        b[3, 1] = 2
        b[3, 2] = 4
        b[3, 3] = 2

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 7
        expected_result[1, 2] = 5.5
        expected_result[1, 3] = 12
        expected_result[2, 1] = 1.5
        expected_result[2, 2] = 1
        expected_result[2, 3] = 2
        expected_result[3, 1] = 10
        expected_result[3, 2] = 4.5
        expected_result[3, 3] = 2

        self.assertTrue(compare_2d_arrays(a.log(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.log(2), expected_result))

    def test_exp(self):
        a = BaseArray((3, 3))
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 1
        a[2, 2] = 1
        a[2, 3] = 2
        a[3, 1] = 3
        a[3, 2] = 2
        a[3, 3] = 3

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 1
        expected_result[1, 2] = 4
        expected_result[1, 3] = 9
        expected_result[2, 1] = 1
        expected_result[2, 2] = 1
        expected_result[2, 3] = 4
        expected_result[3, 1] = 9
        expected_result[3, 2] = 4
        expected_result[3, 3] = 9

        self.assertTrue(compare_2d_arrays(a.exp(2), expected_result))
        self.assertFalse(compare_2d_arrays(a.exp(5), expected_result))

        # =====================================================================

        b = BaseArray((3, 3))
        b[1, 1] = 2
        b[1, 2] = 10
        b[1, 3] = 5
        b[2, 1] = 3
        b[2, 2] = 0
        b[2, 3] = 1
        b[3, 1] = 9
        b[3, 2] = 2
        b[3, 3] = 5

        expected_result = BaseArray((3, 3))
        expected_result[1, 1] = 1
        expected_result[1, 2] = 1024
        expected_result[1, 3] = 243
        expected_result[2, 1] = 1
        expected_result[2, 2] = 1
        expected_result[2, 3] = 2
        expected_result[3, 1] = 19683
        expected_result[3, 2] = 4
        expected_result[3, 3] = 243

        self.assertTrue(compare_2d_arrays(a.exp(b), expected_result))
        self.assertFalse(compare_2d_arrays(a.exp(2), expected_result))
