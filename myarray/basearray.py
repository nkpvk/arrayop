# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array
import math
import copy

def insert_sort(arr):
    i = 1
    while i < len(arr):
        x = arr[i]
        j = i - 1
        while j >= 0 and arr[j] > x:
            arr[j + 1] = arr[j]
            j = j - 1
        arr[j + 1] = x
        i = i + 1

def msort(x):
    result = []
    if len(x) < 2:
        return x
    if len(x) <= 50:
        insert_sort(x)
        return x
    mid = int(len(x)/2)
    y = msort(x[:mid])
    z = msort(x[mid:])
    while (len(y) > 0) or (len(z) > 0):
        if len(y) > 0 and len(z) > 0:
            if y[0] > z[0]:
                result.append(z[0])
                z.pop(0)
            else:
                result.append(y[0])
                y.pop(0)
        elif len(z) > 0:
            for i in z:
                result.append(i)
                z.pop(0)
        else:
            for i in y:
                result.append(i)
                y.pop(0)
    return result

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def to_string(self):
        string = ""
        length = len(self.shape)
        max_length = 0

        for v in self:
            tmp = len(str(v))
            if tmp > max_length:
                max_length = tmp

        if length == 1:
            for col in range(1, self.shape[0]+1):
                string += "{:>{width}}".format(self[col], width=max_length+1)
            string += '\n'
        elif length == 2:
            for row in range(1, self.shape[0]+1):
                col_str = ""
                for col in range(1, self.shape[1]+1):
                    if isinstance(self[row, col], int):
                        col_str += "{:>{width}}".format(self[row, col], width=max_length+1)
                    elif isinstance(self[row, col], float):
                        col_str += "{:>{width}}".format(round(self[row, col], 2), width=7)
                string += col_str + '\n'
        elif length == 3:
            for row in range(1, self.shape[0]+1):
                col_str = ""
                for col in range(1, self.shape[1]+1):
                    for z in range(1, self.shape[2]+1):
                        if isinstance(self[row, col], int):
                            col_str += "{:>{width}}".format(self[row, col], width=max_length+1)
                        elif isinstance(self[row, col], float):
                            col_str += "{:>{width}}".format(round(self[row, col], 2), width=max_length+2)
                string += col_str + '\n'

        return string

    def print(self):
        print(self.to_string())

    def sort(self, row=True):
        result = BaseArray(self.shape)
        length = len(self.shape)
        if row:
            if length == 1:
                tmp = []
                for x in range(1, self.shape[0]+1):
                    tmp.append(self[x])
                tmp = msort(tmp)
                for i in range(1, self.shape[0]+1):
                    result[i] = tmp[i-1]
            elif length == 2:
                for x in range(1, self.shape[0]+1):
                    tmp = []
                    for y in range(1, self.shape[1]+1):
                        tmp.append(self[x, y])
                    tmp = msort(tmp)
                    for i in range(1, self.shape[1]+1):
                        result[x, i] = tmp[i-1]
        else:
            if length == 1:
                tmp = []
                for x in range(1, self.shape[0]+1):
                    tmp.append(self[x])
                tmp = msort(tmp)
                for i in range(1, self.shape[0]+1):
                    result[i] = tmp[i-1]
            elif length == 2:
                for y in range(1, self.shape[1]+1):
                    tmp = []
                    for x in range(1, self.shape[0]+1):
                        tmp.append(self[x, y])
                    tmp = msort(tmp)
                    for i in range(1, self.shape[0]+1):
                        result[i, y] = tmp[i-1]

        return result

    def find(self, value):
        positions = []

        if value in self == False:
            return positions

        length = len(self.shape)
        if length == 1:
            for x in range(1, self.shape[0]+1):
                if self[x] == value:
                    positions.append((x,))
        elif length == 2:
            for x in range(1, self.shape[0]+1):
                for y in range(1, self.shape[1]+1):
                    if self[x, y] == value:
                        positions.append((x, y))
        elif length == 3:
            for x in range(1, self.shape[0]+1):
                for y in range(1, self.shape[1]+1):
                    for z in range(1, self.shape[2]+1):
                        if self[x, y, z] == value:
                            positions.append((x, y, z))

        return positions

    def add(self, value):
        result = copy.deepcopy(self)
        if isinstance(value, int):
            result.__data = list(map(lambda x: x + value, self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if isinstance(value[x, y], int):
                            result[x, y] += int(value[x, y])
                        elif isinstance(value[x, y], float):
                            result[x, y] += float(value[x, y])
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if isinstance(value[x, y, z], int):
                                result[x, y, z] += int(value[x, y, z])
                            elif isinstance(value[x, y, z], float):
                                result[x, y, z] += float(value[x, y, z])

        return result

    def sub(self, value):
        result = copy.deepcopy(self)
        if isinstance(value, int):
            result.__data = list(map(lambda x: x - value, self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if isinstance(value[x, y], int):
                            result[x, y] -= int(value[x, y])
                        elif isinstance(value[x, y], float):
                            result[x, y] -= float(value[x, y])
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if isinstance(value[x, y, z], int):
                                result[x, y, z] -= int(value[x, y, z])
                            elif isinstance(value[x, y, z], float):
                                result[x, y, z] -= float(value[x, y, z])

        return result

    def mult_scalar(self, value):
        result = copy.deepcopy(self)
        if isinstance(value, int):
            result.__data = list(map(lambda x: x + value, self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if isinstance(value[x, y], int):
                            result[x, y] *= int(value[x, y])
                        elif isinstance(value[x, y], float):
                            result[x, y] *= float(value[x, y])
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if isinstance(value[x, y, z], int):
                                result[x, y, z] *= int(value[x, y, z])
                            elif isinstance(value[x, y, z], float):
                                result[x, y, z] *= float(value[x, y, z])

        return result

    def mult(self, value):
        result = copy.deepcopy(self)
        if isinstance(value, int):
            result.__data = list(map(lambda x: x * value, self))
        elif isinstance(value, BaseArray):
            if self.shape[1] != value.shape[0]:
                raise Exception("Cannot multiply matrices. Invalid shape.")
                return
            if len(self.shape) == 2:
                result = BaseArray((self.shape[0], value.shape[1]))
                for i in range(1, self.shape[0]+1):
                    for j in range(1, value.shape[1]+1):
                        for k in range(1, value.shape[0]+1):
                            result[i, j] += self[i, k] * value[k, j]

        return result

    def div(self, value):
        result = copy.deepcopy(self)
        if isinstance(value, int):
            if value == 0:
                return result
            result.__data = list(map(lambda x: x / value, self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if value[x, y] == 0:
                            continue
                        if isinstance(value[x, y], int):
                            result[x, y] /= int(value[x, y])
                        elif isinstance(value[x, y], float):
                            result[x, y] /= float(value[x, y])
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if value[x, y, z] == 0:
                                continue
                            if isinstance(value[x, y, z], int):
                                result[x, y, z] /= int(value[x, y, z])
                            elif isinstance(value[x, y, z], float):
                                result[x, y, z] /= float(value[x, y, z])

        return result

    def log(self, value):
        result = BaseArray(self.shape)
        if isinstance(value, int):
            result.__data = list(map(lambda x: math.log(x)/math.log(value), self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if math.log(value[x, y]) == 0:
                            continue
                        result[x, y] = math.log(self[x, y]) / math.log(value[x, y])
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if math.log(value[x, y, z]) == 0:
                                continue
                            result[x, y, z] = math.log(self[x, y, z]) / math.log(value[x, y, z])

        return result

    def exp(self, value):
        result = BaseArray(self.shape)
        if isinstance(value, int):
            result.__data = list(map(lambda x: math.pow(x, value), self))
        elif isinstance(value, BaseArray):
            if self.shape != value.shape:
                return
            length = len(self.shape)
            if length == 2:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        if isinstance(self[x, y], int):
                            result[x, y] = int(math.pow(self[x, y], value[x, y]))
                        elif isinstance(self[x, y], float):
                            result[x, y] = float(math.pow(self[x, y], value[x, y]))
            elif length == 3:
                for x in range(1, self.shape[0]+1):
                    for y in range(1, self.shape[1]+1):
                        for z in range(1, self.shape[2]+1):
                            if isinstance(self[x, y, z], int):
                                result[x, y, z] = int(math.pow(self[x, y, z], value[x, y, z]))
                            elif isinstance(self[x, y, z], float):
                                result[x, y, z] = float(math.pow(self[x, y, z], value[x, y, z]))

        return result

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
